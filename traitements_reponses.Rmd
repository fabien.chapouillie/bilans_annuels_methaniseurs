---
title: "Exploitation Démarches simplifiées : traitements des réponses"
author: "Juliette Engelaere-Lefebvre"
date: "01/03/2021"
output: 
  html_document:
    df_print: paged
    toc: TRUE
    toc_depth: 4
    toc_float: TRUE
    css: redressmts_geo/gouvdown.css
    keep_md: FALSE
params:
  sgbd: TRUE
  campagne: "2019"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
library(COGiter)
library(readODS)
library(readxl)
library(tricky)
library(lubridate)
library(forcats)

# Chargement des réponses assemblées et des données géo consolidées par TEO
load("exports.RData")
load("exports_geo.RData")
```

# Ensemble des réponses

## Premiers redressements et créations de variables de groupements/d'exclusion

Redressement de la **typologie de l'installation** : comparaison typologie des unités AILE et typologie déclarée pour harmonisation  : besoin de la typologie de référence AILE fournie aujourd'hui dans le tableurs redressé des intrants.  

> Incomplet : ne concerne que les installations qui ont fourni un tableur intrant, les libellés de typologie différents entre enquête et base AILE -> besoin d'une table de passage unique.  

> Note pour plus tard : la typologie des installations ne doit pas être demandée si on a l'info par ailleurs. A tout le moins il faut informer les exploitants de quelle famille ils relèvent in fine pour qu'ils puissent se situer dans le rapport et qu'il répondent correctement la fois d'après.  

Dans la suite des traitements, les installations seront parfois regroupées par **famille** : ensemble les unités « à la ferme », « centralisé », « collectif agricole » / IAA / STEP / ISDND. (4 familles)

Pour certains calculs, les **unités mises en service l’année du bilan** seront exclues. Pour cela on se base sur le champ `date_de_mise_en_service_de_l_installation`, ou si vide de l'année de la `date_de_prise_d_effet_du_contrat`.

De même il convient d'identifier les installations ayant subi des **dysfonctionnements** durant l'année écoulée pour les écarter de certains calculs d'indicateurs.   

>  Cette identification est manuelle. Elle se fait à la lecture des réponses à la question sur les éventuels dysfonctionnements et est reportée dans la la table de passage.

> Redressement de la **typologie de valorisation** : Aile a repris le type de valo de 3 ISDND d'injection à cogénération ?  
cf onglet reponses du fichier _resultats_bilans_cogeneration.xlsx_  
Implémentation manuelle, à tracer plus proprement à l'avenir (info non reprise dans l'onglet de travail, pas toutes en bleu...).
--> **Toutes les ISDND sont en cogénération**.

Enfin des valeurs ont été **corrigées à la main suite à des relances téléphoniques**, à l'avenir il faudra les tracer dans un journal à part plutôt que de les mettre en couleur.  
Seule modification de ce type détectée : le volume de biogaz en entrée de cogénérateur est passé à 850000 m3 pour l'installation 225 METHABATES à Mauges sur Loire.  


    
```{r familles, message=FALSE}
# table de correspondances entre les libellés de typo AILE et DS
lib_typo <- data.frame(lib_DS = c("à la ferme", "centralisée multi-acteurs", "collectif agricole", "industrielle",
                                 "STEP", "ISDND"),
                      lib_aile = c("A la ferme", "Centralisée", "Collectif agricole", "IAA", "STEP", "ISDND"))

# lecture des typologies d'installations revues dans le tableur intrant
typologie_aile <- read_excel("procedure_aile/Resultats_Bilan_INTRANTS.xlsx", sheet = "Intrants_REAL_Cor", 
                             col_types = "text") %>% 
  set_standard_names() %>% 
  select(id_ds, id_aile, typo_aile) %>% 
  distinct() %>% 
  mutate(lib_aile = na_if(typo_aile, "0"),
         # on redresse le champ des identifiants
         id_ds = if_else(nchar(id_ds) == 1, NA_character_, id_ds)) %>% 
  # on renomme les typo pour qu'elle matchent entre DS et AILE
  left_join(lib_typo, by = "lib_aile") %>% 
  select(-lib_aile, -typo_aile) %>% 
  rename(typo_revu_AILE = lib_DS)

# listes des identifiants aile d'installations à traiter comme cogé au lieu de injection
coge_cor_aile <- c("552", "553", "104", "766")

# ajout de la typologie corrigée à la table des réponses, implémentation des corrections téléphoniques et de valorisation
compil_rep_1 <- compil_reponses_sans_RGPD %>% 
  full_join(typologie_aile, by = c("id_DS" = "id_ds", "id_aile")) %>% 
  mutate(type_instal = coalesce(typo_revu_AILE, type_d_installation),
         # correction téléphonique de valeurs
         quantite_annuelle_de_biogaz_en_entree_de_cogenerateur_en_m3_ = case_when(
           id_aile == "225" ~ "850000",
           TRUE ~ quantite_annuelle_de_biogaz_en_entree_de_cogenerateur_en_m3_ ),
         type_de_valorisation = case_when(
           id_aile %in% coge_cor_aile ~ "cogénération",
           TRUE ~type_de_valorisation))
   
# liste des installations dont la typologie a été revue      
inst_typo_revue <- filter(compil_rep_1, (type_instal != type_d_installation)| (id_aile %in% coge_cor_aile)) %>% 
  select(id_aile, entreprise_raison_sociale, type_declare = type_d_installation , type_retenu = type_instal,
         type_de_valorisation)

# création des familles de groupements, et des booléens identifiant les années incomplètes ou anormales
compil_rep_2 <- compil_rep_1 %>% 
  select(-typo_revu_AILE, -type_d_installation) %>% 
  mutate(date_de_prise_d_effet_du_contrat = ymd(date_de_prise_d_effet_du_contrat),
         annee_mes = coalesce(year(date_de_mise_en_service_de_l_installation), year(date_de_prise_d_effet_du_contrat)),
         mes_bilan = (annee_mes>=params$campagne),
         famille = as.factor(type_instal) %>%
           fct_collapse("agricole ou centralisé" = c("à la ferme", "centralisée multi-acteurs", "collectif agricole")),
         dysfonctionnement = (dysfonctionnement == "oui")) %>% 
  rename(type_valo = type_de_valorisation)
```

Liste des installations dont la typologie a été corrigée grâce aux données AILE :
```{r}
inst_typo_revue %>% arrange(type_retenu)
```

> D'où provient l'info sur l'augmentation de puissance en `r params$campagne` (colonne O) ?

```{r augmentation de puissance}
# identifiant AILE des installations ayant augmenté leur puissance l'année du bilan
installations_avec_augm_puissance <- c("361")

```


## Exploration du nombre de réponses

Pour l'instant, on décompte les réponses avec la typologie auto-déclarée pour les installations hors injections.

```{r nb rep par famille}
# liste des variables à sélectionner pour identifier l'installation et ses familles de rattachement
var_ident <- c("id_aile", "id_DS", "type_valo", "type_instal", "famille", "mes_bilan", "dysfonctionnement")

compil_rep_2 %>% group_by(type_instal) %>% 
  summarise(nb_reponses = n_distinct(entreprise_raison_sociale), nb_mes_annee = sum(mes_bilan),
            nb_dysfonctmt = sum(dysfonctionnement, na.rm = TRUE))

```


## Redressement volumes produits - volumes torchés - part CH4 - énergie primaire

Le volume de biogaz produit est une information demandée mais pas toujours correctement renseignée. Les traitements opérés pour le déterminer sont :  

1. sélection de la valeur strictement positive : quantite_annuelle_de_biogaz_produit_en_m3_,  
2. sinon quantite_annuelle_de_biogaz_en_entree_de_cogenerateur_en_m3_ + volume_torche_m3,  
3. sinon quantite_annuelle_de_biogaz_en_entree_de_cogenerateur_en_m3_,  
4. sinon quantite_annuelle_de_biogaz_traite_par_le_systeme_d_epuration_en_nm3_ + volume_torche_m3,   
5. sinon quantite_annuelle_de_biogaz_traite_par_le_systeme_d_epuration_en_nm3_

Toutes les quantités annuelles de biogaz déclarées ne sont utilisées pour réaliser ces calculs que si strictement positives.

On compare ensuite ce volume produit au tonnage entrant total pour détecter des éventuels erreurs d'unité.

Pour le volume torché, il est nécessaire de vérifier si la valeur déclarée au niveau du débit de la torchère (Nm3/heure) n’est pas en fait celle du volume de biogaz torché (m3) : par exemple en 2019, quelques unités ont déclaré un volume torché au niveau des champs `nombre_d_heures_de_fonctionnement_de_la_torchere` ou `debit_de_la_torchere.`

La valeur indiquée au niveau du débit de la torchère est détectée comme un volume si le champ comprend les informations textuelles "Volume", "m3" ou si la valeur numérique fournie est supérieure à **2000**.  

On compare ensuite le volume torché au volume de biogaz produit.

> En cas de discordance entre le volume torché calculé avec le débit et le nombre d'heure de fonctionnement de la torchère et par différence entre les m3 annuels de biogaz produit et en entrée de cogenerateur, quelle valeur retient-on ? (aujourd'hui la différence entre m3 prime)

> Comment est calculé le % de biogaz torché ? par rapport au volume total de biogaz produit ou par rapport au volume de biogaz en entrée de process de cogé ou d'épuration (aujourd'hui volume en entrée de process) ?


```{r torchere volumes ep, message=FALSE, warning=FALSE}
seuil_debit_volume <- 2000

cor_val_num <- function(.x, strict = TRUE) {
  if(strict) {as.numeric(.x) %>% if_else(. <= 0, NA_real_, .)} else
  {as.numeric(.x) %>% if_else(. < 0, NA_real_, .)}
  
}

prod_torchere <- select(compil_rep_2, all_of(var_ident), contains("torchere"), presence_d_un_debimetre_de_biogaz,
                        quantite_annuelle_de_biogaz_produit_en_m3_,
                        quantite_annuelle_de_biogaz_en_entree_de_cogenerateur_en_m3_,
                        quantite_annuelle_de_biogaz_traite_par_le_systeme_d_epuration_en_nm3_,
                        taux_de_ch4_dans_le_biogaz_, tonnage_total
                        ) %>% 
  rowwise() %>% 
  mutate(across(c(taux_de_ch4_dans_le_biogaz_, starts_with("quantite_")), cor_val_num ),
         nb_h_torchere = cor_val_num(nombre_d_heures_de_fonctionnement_de_la_torchere, strict = TRUE),
         debit_de_la_torchere = gsub(" |:", "", debit_de_la_torchere) %>% 
           gsub("nm3/h|m3/h", "", .),
         debit_torch = cor_val_num(debit_de_la_torchere, strict = TRUE) %>% if_else(. > seuil_debit_volume, NA_real_, .),
         volume_torche_decl_m3 = if_else(any(grepl("m3|volume|Volume", debit_de_la_torchere),
                                             as.numeric(debit_de_la_torchere) > seuil_debit_volume), 
                                         debit_de_la_torchere, NA_character_) %>% 
           gsub("m3|volume|Volume", "", .) %>% as.numeric,
         volume_torche_m3 = coalesce(
           cor_val_num(quantite_annuelle_de_biogaz_produit_en_m3_ -
                         quantite_annuelle_de_biogaz_en_entree_de_cogenerateur_en_m3_),
           volume_torche_decl_m3, 
           debit_torch * nb_h_torchere),
         biogaz_prod_m3 = coalesce(quantite_annuelle_de_biogaz_produit_en_m3_,
                                   quantite_annuelle_de_biogaz_en_entree_de_cogenerateur_en_m3_ + volume_torche_m3,
                                   quantite_annuelle_de_biogaz_en_entree_de_cogenerateur_en_m3_,
                                   quantite_annuelle_de_biogaz_traite_par_le_systeme_d_epuration_en_nm3_ + volume_torche_m3,
                                   quantite_annuelle_de_biogaz_traite_par_le_systeme_d_epuration_en_nm3_),
         # correction des litres en m3
         m3_par_t_intrants = (biogaz_prod_m3 / cor_val_num(tonnage_total)) %>% round(1),
         biogaz_prod_m3 = if_else(!is.na(m3_par_t_intrants) & m3_par_t_intrants>75000, biogaz_prod_m3/1000, biogaz_prod_m3),
         m3_entree_coge = if_else(!grepl("cog", type_valo), NA_real_, 
                                  coalesce(quantite_annuelle_de_biogaz_en_entree_de_cogenerateur_en_m3_, 
                                           quantite_annuelle_de_biogaz_produit_en_m3_ - volume_torche_m3,
                                           quantite_annuelle_de_biogaz_produit_en_m3_)) %>% 
           # correction des litres en m3
           if_else((. / cor_val_num(tonnage_total))>75000 & !is.na(cor_val_num(tonnage_total)), ./1000, .),
         m3_entree_epur = if_else(!grepl("inj", type_valo), NA_real_,
                                  coalesce(quantite_annuelle_de_biogaz_traite_par_le_systeme_d_epuration_en_nm3_, 
                                           quantite_annuelle_de_biogaz_produit_en_m3_ - volume_torche_m3,
                                           quantite_annuelle_de_biogaz_produit_en_m3_)) %>% 
            # correction des litres en m3
           if_else((. / cor_val_num(tonnage_total))>75000 & !is.na(cor_val_num(tonnage_total)), ./1000, .),
         m3_entree_process = coalesce(m3_entree_coge, m3_entree_epur),
         part_torche = volume_torche_m3/m3_entree_process*100,
         part_ch4 = case_when(famille == "agricole ou centralisé" & taux_de_ch4_dans_le_biogaz_ < 48 ~ NA_real_,
                              famille == "agricole ou centralisé" & taux_de_ch4_dans_le_biogaz_ > 62 ~ NA_real_,
                              TRUE ~ taux_de_ch4_dans_le_biogaz_),
         energie_primaire_tot = biogaz_prod_m3 * part_ch4/100 * 9.94,
         energie_primaire_entree_coge = m3_entree_coge * part_ch4/100 * 9.94) %>% 
  ungroup()


var_inutiles <- setdiff(names(compil_rep_2), var_ident)
prod_torchere_2 <- select(prod_torchere, -any_of(var_inutiles), -nb_h_torchere, -debit_torch, -volume_torche_decl_m3,
                          -m3_par_t_intrants, -m3_entree_process)

```

Nombre d'installations pour lesquelles on n'a pas le volume biogaz produit : `r filter(prod_torchere, is.na(biogaz_prod_m3)) %>% nrow()`.

Pour redresser le champ `taux_de_ch4_dans_le_biogaz_`, AILE nous indique que la valeur est acceptable si comprise entre 48% et 62% pour les installations agricoles ou centralisées, et que toutes les valeurs sont acceptables pour les ISDND, les STEP ou les installations industrielles.    

> Le taux de CH4 fourni par l'installation industrielle 150 COOPERATIVE DES PRODUCTEURS LEGUMIERS, de 34 %, est à invalider ? 

# Cogénération
On s'intéresse maintenant aux caractéristiques des seules installations de cogénération.  

Nombre d'installations en cogénération : `r filter(compil_rep_2, grepl("og", type_valo)) %>% nrow()`.

## Type de valorisation de la chaleur.
A partir des informations contenues dans le champ `chaleur_valorisee_hors_process_type_de_valorisation_quantite_en_kwh_`, il faut produire 2 nouveaux champs : l'un contenant les valeurs numériques, l'autre indiquant quel est le type d'usage de la chaleur valorisée.  

Aperçu du champ à redresser : 

```{r apercu valo chaleur}
filter(compil_rep_2, nchar(chaleur_valorisee_hors_process_type_de_valorisation_quantite_en_kwh_)>2,
       grepl("og", type_valo)) %>% 
  pull(chaleur_valorisee_hors_process_type_de_valorisation_quantite_en_kwh_) %>% 
  head(10)
```

 
Aile possède une nomenclature des usages de valorisation de la chaleur qui n'a pas été intégrée aux questions posées. Il s'agit de rattacher les commentaires fournis à une famille à partir de quelques expressions clés.

### Construction de la table des usages de la chaleur valorisées et des termes clé associés
```{r typo usage}

typo_usage_chal <- list(
  elevage = list( lib = "Chauffage bâtiments d'élevage", 
                  exp = c("poulailler", "volaille", "ferme", "exploitation agricole", "porcherie", "élevage", "avicole")),
  chauffage = list(lib ="Chauffage domestique",
                   exp = c("maison")), 
  sechoir = list(lib ="Séchoir",
                 exp = c("séchoir")),
  vente = list(lib = "Vente industriel",
               exp = c("vente", "vendu", "voisin"))
  )

# Une fonction qui renvoie TRUE si l'un des termes clés est détectée dans le commentaires
detect <- function(usage = "elevage", champ = .data$chaleur_valo_0) {
  pattern <- paste(typo_usage_chal[[usage]]$exp, collapse = "|")
  grepl(pattern, tolower(champ))
}

```


### Redressements divers liés à la chaleur valorisée 
```{r}
coge_chal <- select(compil_rep_2, all_of(var_ident),
                    chaleur_valo_0 = chaleur_valorisee_hors_process_type_de_valorisation_quantite_en_kwh_) %>% 
  # on ne garde que les cogés
  filter(grepl("og", type_valo)) %>% 
  # création du champ numérique
  mutate(chal_valo_kwh = tolower(chaleur_valo_0) %>% # tout en minuscule
           gsub(" kw", "kw", .) %>% # on colle la valeur à son unité
           gsub("(?<=\\d) (?=\\d\\d\\d)", "", ., perl = TRUE)) %>% # on enlève les espaces séparateurs de milliers entre les chiffres
  rowwise %>% 
  mutate(chal_valo_kwh = case_when(
    # on prend tous les chiffres avant kw
    grepl("kw", chal_valo_kwh) ~ str_extract(chal_valo_kwh, "\\d*(?=kw)"),
    # sinon on prend tous les nombres présents s'ils ont plus de 5 chiffres
    grepl("\\d{5}", chal_valo_kwh) ~ str_extract(chal_valo_kwh, "\\d{5,}"),
    # sinon, si la case ne contient que des chiffres, on prend la case complete
    !grepl("[[:alpha:]]", chal_valo_kwh) ~ chal_valo_kwh,
    TRUE ~ NA_character_) %>% 
      as.numeric,
    # création du champ type usage de la chaleur
    chal_valo_usage = case_when(
      # recherche des mots clés
      detect(usage = "elevage", champ = chaleur_valo_0) ~ typo_usage_chal$elevage$lib,
      detect(usage = "chauffage", champ = chaleur_valo_0) ~ typo_usage_chal$chauffage$lib,
      detect(usage = "sechoir", champ = chaleur_valo_0) ~ typo_usage_chal$sechoir$lib,
      detect(usage = "vente", champ = chaleur_valo_0) ~ typo_usage_chal$vente$lib,
      # pas d'usage si pas de valeur
      is.na(chal_valo_kwh) | chal_valo_kwh == 0 ~ NA_character_,
      TRUE ~ "Autres"),
    chal_valo = case_when(chal_valo_kwh == 0 ~ FALSE,
                          chal_valo_kwh > 0 ~ TRUE,
                          TRUE ~ NA)
  ) %>% 
  ungroup()

```

> d'où vient la colonne AA Valorisation de la chaleur (oui/non) ? (implémentée à partir du volume de chaleur indiqué dans `chaleur_valorisee_hors_process_type_de_valorisation_quantite_en_kwh_`)


## Redressement production élec - chaleur et efficacité énergétique

> D'où vient le redressement de la production électrique injectée sur le réseau de    
> l'installation 343 EARL L’OUVRINIERE à segré en anjou bleu ?  (passage de 1 023 192 kWh à 142 418 kWh)

> D'où vient le redressement de la puissance électrique de    
> l'installation 104 ISDND à Saint Fraimbault ?  (passage de 10 761 660 kW à 1 415 kW, registre 1 365)


```{r elec chaleur EE, message=FALSE, warning=FALSE}
is_not_empty <- function(x) any(!is.na(x))

elec_chal_ee <- select(compil_rep_2, any_of(var_ident), contains("elec"), puiss_MW_registre, contains("chaleur"), 
                       contains("kwh"), -contains("date")) %>% 
  filter(grepl("og", type_valo)) %>% 
  select(where(is_not_empty)) %>% 
  # ajout colonnes redressees chaleur valorisée
  left_join(select(coge_chal, -chaleur_valo_0)) %>% 
  # ajout energie_primaire_entree_coge
  left_join(select(prod_torchere, id_aile, id_DS, energie_primaire_entree_coge)) %>% 
  mutate(
    across(c(puissance_electrique_en_kwe_:prod_elec_MWh_an_registre), ~ as.numeric(.x) %>% cor_val_num()),
    # élimination de valeurs aberrantes
    production_electrique_injectee_sur_le_reseau_en_kwh_ = case_when(
      production_electrique_injectee_sur_le_reseau_en_kwh_<3 ~ NA_real_,
      id_aile == "343" ~ 142418,
      TRUE ~ production_electrique_injectee_sur_le_reseau_en_kwh_),
    puissance_electrique_en_kwe_ = if_else(id_aile == "104", 1415, puissance_electrique_en_kwe_),
    ratio_chal_elec = chal_valo_kwh / production_electrique_injectee_sur_le_reseau_en_kwh_ * 100,
    ee = ((chal_valo_kwh + production_electrique_injectee_sur_le_reseau_en_kwh_)/energie_primaire_entree_coge * 100) %>% round(1),
    elec_prod_decl = production_electrique_injectee_sur_le_reseau_en_kwh_+ consommation_electrique_des_auxiliaires_du_moteur_de_cogeneration_en_kwh_,
    elec_prod_estim = (production_electrique_injectee_sur_le_reseau_en_kwh_ * (1 + 1/19)) %>% round(1), # estim de conso aux à 5% si NA
    poids_conso_aux = (consommation_electrique_des_auxiliaires_du_moteur_de_cogeneration_en_kwh_/elec_prod_decl*100) %>% round(1),
    elec_prod = case_when(
      is.na(consommation_electrique_des_auxiliaires_du_moteur_de_cogeneration_en_kwh_) ~ elec_prod_estim,
      poids_conso_aux > 10 | poids_conso_aux < 2 ~ elec_prod_estim,
      TRUE ~ elec_prod_decl),
    rendmt_moteur = (elec_prod / energie_primaire_entree_coge * 100) %>% round(1),
    poids_conso_process = consommation_electrique_de_l_installation_pour_le_process_de_methanisation_en_kwh_ / 
      production_electrique_injectee_sur_le_reseau_en_kwh_,
    nb_h_eq_pleine_puiss = (elec_prod / puissance_electrique_en_kwe_) %>% round(0))

```

IL reste à éliminer les éventuelles valeurs aberrantes.
